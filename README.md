# MyFinanceApp

![App Icon](./appicon.png)

This is a sample *Xcode project* created to demonstrate developer's skills, approaches, knowledges about *iOS development*.

## Basic demands

| № | Functionality | Test | Implementation |
| --- | --- | --- | --- |
1 | `Добавление в список пару валют` | ✅ | ✅ |
2 | `Удаление из списка пару валют` | ✅ | ✅ |
3 | `Сохранение списка пар валют локально` | ✅ | ✅ |
4 | `Асинхронное обновление пар валют с API` | ✅ | ✅ |

## Additional demands

* ✅ `Приложение написано с использованием последних стабильных версий Xcode и Swift`
* ✅ `Не используются WebView и кросс-платформенные фреймворки`
* ✅ `Код следует Swift API Design Guidelines и одному из поддерживаемых коммьюнити стайл-гайдов`
* ✅ `Поддерживается все iPhone и обе ориентации экрана`
* ✅ `Написанный код **не** использует дополнительные библиотеки`
* ✅ `Написанный код покрыт unit-тестами`

## My tech stack list

| Aspect | Option |
| --- | --- |
| IDE | Xcode `10.2.1` |
| Programming language | Swift `5.0.1` |
| Version control system | Git `2.19.2` |
| Architecture pattern | My own [modified MVVM](https://github.com/shokuroff/MVVMSR) |
| Test framework | XCTest |
| Commits design | [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) |

## Gotta try

1. `git clone https://shokuroff@bitbucket.org/shokuroff/myfinancetestapp.git ~/Desktop/shokuroff/`
2. `cd ~/Desktop/shokuroff`
3. `open -a /Applications/Xcode.app MyFinancialApp`
4. `CMD + U` to test.
5. `CMD + R` to run.

**P.S.** Step number 4 may not work because of a [bug](https://github.com/CocoaPods/CocoaPods/issues/8139) I've found recently however there is a [tech solution](https://github.com/CocoaPods/CocoaPods/issues/8139#issuecomment-433679242) by [@armcknight](https://github.com/armcknight).