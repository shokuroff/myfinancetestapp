//
//  Currency.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//




import Foundation



struct Currency: Equatable, Comparable {
    
    
    // MARK: – Properties
    
    let code: String
    let name: String
    
    
    
    // MARK: – Equatable
    
    static func ==(lhs: Currency, rhs: Currency) -> Bool {
        return lhs.code == rhs.code && lhs.name == rhs.name
    }
    
    
    
    // MARK: – Comparable
    
    static func <(lhs: Currency, rhs: Currency) -> Bool {
        return lhs.code < rhs.code // B will be preceded by A, or USD by RUB, etc.
    }
    
    
}
