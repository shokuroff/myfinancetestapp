//
//  SavedCurrency.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



class Rate {
    
    
    // MARK: – Properties
    
    let baseCurrency: Currency
    let secondCurrency: Currency
    var relativeRatio: Float
    
    // For Cell View:
    var loading: Bool = false
    
    
    
    
    // MARK: – Initialization
    
    init(baseCurrency: Currency, secondCurrency: Currency, relativeValue: Float) {
        self.baseCurrency = baseCurrency
        self.secondCurrency = secondCurrency
        self.relativeRatio = relativeValue
    }

    
}





// MARK: – Equatable

extension Rate: Equatable {
    
    static func ==(lhs: Rate, rhs: Rate) -> Bool {
        return lhs.baseCurrency == rhs.baseCurrency && lhs.secondCurrency == rhs.secondCurrency
    }
    
}
