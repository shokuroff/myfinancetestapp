//
//  CDRate+CoreDataProperties.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/7/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//
//

import Foundation
import CoreData


extension CDRate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDRate> {
        return NSFetchRequest<CDRate>(entityName: "CDRate")
    }

    @NSManaged public var baseCurrencyCode: String?
    @NSManaged public var baseCurrencyName: String?
    @NSManaged public var secondCurrencyCode: String?
    @NSManaged public var secondCurrencyName: String?
    @NSManaged public var relativeFloatValue: Float

}
