//
//  CoreDataStack.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation
import CoreData



class CoreDataStack {
    
    
    typealias ErrorDescription = (title: String, message: String?)
    typealias FailureHandler = ((_ failure: Bool, _ error: ErrorDescription?) -> Void)
    
    
    
    // MARK: – Properties
    
    private let modelName: String
    
    private var temporarySelectedBaseCurrency: Currency?
    private var temporarySelectedSecondCurrency: Currency?
    
    
    
    
    
    // MARK: – Entities
    
    private let rateEntityName = "CDRate"
    private let currencyEntityName = "CDCurrency"
    
    
    
    
    
    // MARK: – Object Life Cycle
    
    init(modelName: String) {
        self.modelName = modelName
        if self.currencyListEmpty {
            self.loadAndSaveCurrenciesFromRawTextFileInBundle()
        }
        #if DEBUG
        print("\n[\(self) init] SUCCESS.\n")
        #endif
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Store Container
    
    lazy var storeContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores(completionHandler: {description, error in
            if error != nil {
                #if DEBUG
                print("[\(self) storeContainer.loadPersistentStores] \(error!.localizedDescription)")
                #endif
            }
        })
        return container
    }()
    
    
    
    
    
    // MARK: – Context
    
    lazy var context = self.storeContainer.viewContext
    
    
    
    
    
    // MARK: – Saving
    
    /// Each time it needs to save changes in `NSManagedObjectContext` instance, use this method. 👆
    
    final func tryToSave() {
        guard self.context.hasChanges else {
            return // no changes to save. 👍🏻
        }
        do {
            try self.context.save()
        } catch {
            #if DEBUG
            print("\n[\(self) tryToSave] \(error.localizedDescription)\n")
            #endif
        }
    }
    
    
    
    
    
    // MARK: – Currency
    
    /// Use this method to deselect base currency selection
    /// - Note: It's possible when base currency list view controlelr closes switching the user back to the main screen with rates list.
    
    final func deselectBaseCurrency() {
        self.temporarySelectedBaseCurrency = nil
    }
    
    
    
    /// Use this method to deselect second currency selection
    /// - Note: It's possible when second currency list view controller is gonna deinitialize itself 🤖.
    
    final func deselectSecondCurrency() {
        self.temporarySelectedSecondCurrency = nil
    }
    
    
    
    /// This read-only property outputs the number of CDCurrency records in .xcdatamodel file.
    /// - Note: Basically, the default value is equal to `true` because of empty list when app has only just been installed by user.
    
    final var currencyListEmpty: Bool {
        get {
            let request: NSFetchRequest<CDCurrency> = CDCurrency.fetchRequest()
            do {
                return try self.context.count(for: request) <= 0 ? true : false
            } catch {
                #if DEBUG
                print("\n[\(self) currencyListEmpty] \(error.localizedDescription)\n")
                #endif
                return true
            }
        }
    }
    
    
    
    // A read-only property with a list of currencies with relevant description.
    
    final var currencies: [Currency] {
        get {
            let request: NSFetchRequest<CDCurrency> = CDCurrency.fetchRequest()
            do {
                let rawResult = try self.context.fetch(request)
                var result: [Currency] = []
                for rawItem in rawResult {
                    let errorMessage = "CoreDataStack.swift: line #144"
                    let code = rawItem.code ?? errorMessage; let name = rawItem.name ?? errorMessage
                    let loadedCurrency = Currency(code: code, name: name)
                    result.append(loadedCurrency)
                }
                return result.sorted()
            } catch {
                #if DEBUG
                print("\n[\(self) currencies] \(error.localizedDescription)\n")
                #endif
                return []
            }
        }
    }
    
    
    
    /// This method is calling whenever it needs to load a currency list from a raw text file
    /// or when `currencyListEmpty` property has a value `true` basically.
    
    private func loadAndSaveCurrenciesFromRawTextFileInBundle() {
        let filePathString = Bundle.main.path(forResource: "Currencies", ofType: "txt")!
        let fullFilePathString = "file://\(filePathString)"
        let filePath = URL(string: fullFilePathString)!
        let rawContentsString = try! String(contentsOf: filePath, encoding: .utf8)
        let rawLines = rawContentsString.components(separatedBy: .newlines)
        var currenciesToSave: [Currency] = []
        rawLines.forEach {
            let firstSpace = $0.firstIndex(of: " ") ?? $0.endIndex
            let code = String($0[..<firstSpace])
            if let range = $0.range(of: "- ") {
                let name = String($0[range.upperBound...])
                let newCurrencyToSave = Currency(code: code, name: name)
                currenciesToSave.append(newCurrencyToSave)
            }
        }
        guard !currenciesToSave.isEmpty else {
            #if DEBUG
            print("\n[\(self) loadAndSaveCurrenciesFromRawTextFileInBundle] Nothing to save from text file.\n")
            #endif
            return
        }
        let entityDescription = NSEntityDescription.entity(forEntityName: self.currencyEntityName, in: self.context)!
        currenciesToSave.forEach { currency in
            let cdCurrency = CDCurrency(entity: entityDescription, insertInto: self.context)
            cdCurrency.code = currency.code
            cdCurrency.name = currency.name
        }
        self.tryToSave()
    }
    
    
    
    /// Use this method when it's necessary to remove all currencies from `CDCurrency` entity.
    
    final func clearAllCurrencies() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.currencyEntityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try self.context.execute(deleteRequest)
            self.tryToSave()
        } catch {
            #if DEBUG
            print("\n[\(self) loadAndSaveCurrenciesFromRawTextFileInBundle] Nothing to save from text file.\n")
            #endif
            return
        }
    }
    
    
    
    /// Use this method to fix user's selection of base currency.
    
    final func fixBaseCurrency(_ currency: Currency?) {
        self.temporarySelectedBaseCurrency = currency
    }
    
    
    
    /// Use this method to fix user's selection of second currency.
    
    final func fixSecondCurrency(_ currency: Currency?) {
        self.temporarySelectedSecondCurrency = currency
    }
    
    
    
    /// Use this method when it needs to save two temporary selected currencies in one new user's rate.
    
    final func tryToSaveNewRate(andDoAfterCompletion callback: @escaping FailureHandler) {
        guard let base = self.temporarySelectedBaseCurrency, let second = self.temporarySelectedSecondCurrency else {
            let errorMessage = "Some of temporary selected currencies are nil, damn. 👎🏻"
            #if DEBUG
            print("\n[\(self) tryToSaveNewRate] \(errorMessage)\n")
            #endif
            callback(true, (String.failureWithSavingNewRateTitle, errorMessage))
            return
        }
        let rate = Rate(baseCurrency: base, secondCurrency: second, relativeValue: 0)
        self.saveNewRate(rate, callback: { failure, error in
            callback(failure, error)
        })
    }
    
    
    
    
    
    // MARK: – Rate
    
    // A read-only property with a list of user's rates.
    
    final var rates: [Rate] {
        get {
            let request: NSFetchRequest<CDRate> = CDRate.fetchRequest()
            do {
                let rawResults = try self.context.fetch(request)
                var result: [Rate] = []
                for cdRate in rawResults {
                    let baseCurrencyCode = cdRate.baseCurrencyCode ?? ""
                    let baseCurrencyName = cdRate.baseCurrencyName ?? ""
                    let secondCurrencyCode = cdRate.secondCurrencyCode ?? ""
                    let secondCurrencyName = cdRate.secondCurrencyName ?? ""
                    let relativeRation: Float = cdRate.relativeFloatValue
                    let loading: Bool = true
                    let baseCurrency = Currency(code: baseCurrencyCode, name: baseCurrencyName)
                    let secondCurrency = Currency(code: secondCurrencyCode, name: secondCurrencyName)
                    let loadedRate = Rate(
                        baseCurrency: baseCurrency,
                        secondCurrency: secondCurrency,
                        relativeValue: relativeRation
                    )
                    loadedRate.loading = loading
                    result.append(loadedRate)
                }
                return result
            } catch {
                #if DEBUG
                print("\n[\(self) rates] \(error.localizedDescription)\n")
                #endif
                return []
            }
        }
    }
    
    
    
    /// Use this method to save a new user's rate.
    
    final func saveNewRate(_ incomingRate: Rate, callback: @escaping FailureHandler) {
        guard !self.rates.contains(incomingRate) else {
            let errorMessage = "No need to save a rate (this option is already exists!)"
            #if DEBUG
            print("\n[\(self) saveNewRate] \(errorMessage)\n")
            #endif
            callback(true, (String.failureWithSavingNewRateTitle, errorMessage))
            return
        }
        guard incomingRate.baseCurrency != incomingRate.secondCurrency else {
            let errorMessage = "No need to save a rate with two same currencies..\n\(incomingRate.baseCurrency.code)-\(incomingRate.secondCurrency.code)"
            #if DEBUG
            print("\n[\(self) saveNewRate] \(errorMessage)\n")
            #endif
            callback(true, (String.failureWithSavingNewRateTitle, errorMessage))
            return
        }
        let entityDescription = NSEntityDescription.entity(forEntityName: self.rateEntityName, in: self.context)!
        let newRateToSave = CDRate(entity: entityDescription, insertInto: self.context)
        newRateToSave.baseCurrencyCode = incomingRate.baseCurrency.code
        newRateToSave.baseCurrencyName = incomingRate.baseCurrency.name
        newRateToSave.secondCurrencyCode = incomingRate.secondCurrency.code
        newRateToSave.secondCurrencyName = incomingRate.secondCurrency.name
        newRateToSave.relativeFloatValue = incomingRate.relativeRatio
        self.tryToSave()
        callback(false, nil)
    }
    
    
    
    /// Use this method to remove a specific rate.
    
    final func removeSpecificRate(_ rate: Rate, andDoAfter callback: @escaping (_ updatedRatesList: [Rate]) -> Void) {
        let request: NSFetchRequest<CDRate> = CDRate.fetchRequest()
        do {
            let rawResults = try self.context.fetch(request)
            let ratesToRemove = rawResults.filter {
                ($0.baseCurrencyCode ?? "") == rate.baseCurrency.code &&
                ($0.baseCurrencyName ?? "") == rate.baseCurrency.name &&
                ($0.secondCurrencyCode ?? "") == rate.secondCurrency.code &&
                ($0.secondCurrencyName ?? "") == rate.secondCurrency.name
            }
            for rateToRemove in ratesToRemove {
                self.context.delete(rateToRemove)
            }
            self.tryToSave()
            callback(self.rates)
        } catch {
            print("\n[\(self) removeSpecificRateWithBaseCurrency] \(error.localizedDescription)\n")
            callback(self.rates)
        }
    }
    
    
    
    /// Use this method when it's necessary to remove all user's rates from `CDRate` entity.
    
    final func clearAllRates() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.rateEntityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try self.context.execute(deleteRequest)
            self.tryToSave()
        } catch {
            print("\n[\(self) clearAllRates] \(error.localizedDescription)\n")
        }
    }
    
    
}
