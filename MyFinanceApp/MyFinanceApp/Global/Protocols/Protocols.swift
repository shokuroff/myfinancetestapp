// 
//  Protocols.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



protocol ViewControllerWithCallbacks: class {
    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?)
    func didGetEmptyData()
    func didGetNewData()
}


protocol ViewModelWithCallbacks: class {
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?)
    func didGetEmptyData()
    func didGetNewData()
}


protocol RouterWithCallbacks: class {
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?)
    func didLoadFromNetworkData(_ data: [Any]) // FIXME: Set up the type of received data from network request. 🤖
}
