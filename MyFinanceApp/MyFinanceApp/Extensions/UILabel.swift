//
//  UILabel.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/9/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



extension UILabel {
    
    
    fileprivate var codeCurrencyFont: UIFont? {
        return UIFont(name: "SFProText", size: 17.0)
    }
    
    fileprivate var nameCurrencyFont: UIFont? {
        return UIFont(name: "SFProText", size: 17.0)
    }
    
    
    final internal func addConstraintsForCodeCurrencyLabelInsideContentView(_ superview: UIView) {
        superview.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: 16.0).isActive = true
        self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 16.0).isActive = true
    }
    
    
    final internal func setupTextStyleForCodeCurrencyLabel() {
        self.textColor = .black
        self.font = self.codeCurrencyFont
        self.textAlignment = .left
    }
    
    
    final internal func addConstraintsForNameCurrencyLabelInsideContentView(_ superview: UIView) {
        superview.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: 16.0).isActive = true
        self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 16.0).isActive = true
    }
    
    
    final internal func setupTextStyleForNameCurrencyLabel() {
        self.textColor = UIColor.black.withAlphaComponent(0.4)
        self.font = self.nameCurrencyFont
        self.textAlignment = .left
    }
    
    
}
