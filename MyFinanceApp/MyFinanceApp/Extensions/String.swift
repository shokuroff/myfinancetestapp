//
//  String.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import Foundation



extension String {
    
    
    /// Current initializer tries to create a too much presice string based on a floating value.
    /// - Parameter value: Any variable with floating value.
    /// - Returns: String
    /// ```
    /// let foo: Float = 13.31456
    /// print(String(byRelativeFloatingRatio: foo)) // prints "13.31". 👍🏻
    /// ```
    
    init(byRelativeFloatingRatio value: Float) {
        let s = String(value)
        let dotSymbol = s.firstIndex(of: ".") ?? s.endIndex
        let integer = String(s[..<dotSymbol])
        var longFraction = ""
        var correctFraction = ""
        
        if let range = s.range(of: ".") {
            longFraction = String(s[range.upperBound...])
            let firstFractionSymbol = longFraction.first ?? "0"
            if longFraction.count > 1 {
                let secondFractionSymbolIndex = longFraction.index(after: longFraction.startIndex)
                let secondFractionSymbol = String(longFraction[secondFractionSymbolIndex])
                correctFraction = "\(firstFractionSymbol)\(secondFractionSymbol)"
            } else {
                correctFraction.append(firstFractionSymbol)
                correctFraction.append("0")
            }
        }
        
        if correctFraction.isEmpty && correctFraction == "00" {
            self = "\(integer).00"
        } else {
            let fraction = correctFraction.isEmpty ? "00" : correctFraction
            self = "\(integer).\(fraction)"
        }
    }

    
}






// MARK: – Cell Identifiers

extension String {
    
    static let listCellID = "ListCellViewIdentifier"
    static let addCellID = "AddCellViewIdentifier"
    
}





// MARK: – Segues Identifiers

extension String {
    
    static let showAddViewControllerSegueID = "ShowAddViewControllerSegueIdentifier"
    
}





// MARK: – Error Description Templates

extension String {
    
    static let failureWithSavingNewRateTitle = "Unable to record your options. ☠️"
    
}
