// 
//  ErrorView.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



protocol ErrorViewDelegate: class {
    func didErrorButtonTouch()
}



class ErrorView: UIView {
    
    
    // MARK: – Error View Type
    
    enum ErrorViewType {
        // use when parent view controller state's is equal to 'empty':
        case empty
        // use when parent view controller state's is equal to 'error':
        case error
    }
    
    
    // MARK: – Properties
    
    weak private var parentViewController: ErrorViewDelegate!
    
    
    
    
    
    // MARK: – Interface Builder Outlets
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var subtitleLabel: UILabel!
    @IBOutlet weak private var button: UIButton!
    
    
    
    
    
    // MARK: – Initializer
    
    class func instanceWithTitle(_ title: String,
                                 message: String?,
                                 inside viewController: ErrorViewDelegate,
                                 withType errorViewType: ErrorViewType) -> ErrorView {
        
        let nib = Bundle.main.loadNibNamed("ErrorView", owner: viewController, options: nil)!
        let view = nib.first as! ErrorView
        view.setTitle(title, withMessage: message)
        view.parentViewController = viewController
        view.button.isHidden = errorViewType == .empty
        return view
    }
    
    
    
    
    
    // MARK: – Interface Builder Actions
    
    @IBAction private func buttonAction(_ sender: UIButton) {
        self.parentViewController.didErrorButtonTouch()
    }
    
    
    
    
    
    // MARK: – Accessors
    
    final func setTitle(_ title: String, withMessage message: String?) {
        self.titleLabel.text = title
        self.subtitleLabel.text = message
        self.subtitleLabel.isHidden = message == nil || (message ?? "").isEmpty
    }
    
    
    final func setButtonTitle(_ title: String) {
        if !title.isEmpty {
            self.button.setTitle(title, for: .normal)
        }
    }
    
    
}