// 
//  LoadingContainerView.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



final class LoadingContainerView: UIView {
    
    
    // MARK: – Properties
    
    private var loadingSubview: LoadingView!
    
    
    
    
    
    // MARK: – Preparing sub views
    
    final func prepareLoadingSubviewWithTitle(_ title: String, inside viewController: ResponsiveViewController) {
        self.loadingSubview = LoadingView.instanceInside(viewController, withTitle: title)
        self.addSubViewToItself()
    }
    
    final func prepareLoadingSubviewInside(_ viewController: ResponsiveViewController) {
        self.loadingSubview = LoadingView.defaultInstanceInside(viewController)
        self.addSubViewToItself()
    }
    
    private func addSubViewToItself() {
        if self.loadingSubview != nil {
            self.addSubview(self.loadingSubview)
            self.loadingSubview.frame = self.bounds
            self.loadingSubview.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    
    
    
    
    
    // MARK: – Public Methods
    
    final func runSpinnerAnimation() {
        self.loadingSubview.runAnimation()
    }
    
    final func runSpinnerAnimationWithTitle(_ title: String) {
        self.loadingSubview.runAnimationWithTitle(title)
    }
    
    final func stopSpinnerAnimation() {
        self.loadingSubview.stopAnimation()
    }
    
    
    
    
    
    // MARK: – Class Implementations
    
    class func installLoadingContainerWithTitle(_ title: String, inside viewController: ResponsiveViewController) {
        // 1. Container:
        let container = LoadingContainerView(frame: viewController.view.bounds)
        viewController.view.addSubview(container)
        container.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // 2. Container's sub views:
        container.prepareLoadingSubviewWithTitle(title, inside: viewController)
        viewController.setLoadingContainerView(container)
    }
    
    
}