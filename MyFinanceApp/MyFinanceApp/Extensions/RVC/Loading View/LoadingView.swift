// 
//  LoadingView.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



class LoadingView: UIView {

    // MARK: – Properties
    
    private let defaultLoadingDescription = "Loading.."
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var spinner: UIActivityIndicatorView!
    @IBOutlet weak private var backgroundModuleView: UIView!
    
    
    // MARK: – Rendering
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.backgroundModuleView.layer.masksToBounds = true
        self.backgroundModuleView.layer.cornerRadius = 10.0
    }
    
}






// MARK: – Initializers

extension LoadingView {
    
    
    class func defaultInstanceInside(_ viewController: ResponsiveViewController) -> LoadingView {
        let nib = Bundle.main.loadNibNamed("LoadingView", owner: viewController, options: nil)!
        let view = nib.first as! LoadingView
        view.titleLabel.text = view.defaultLoadingDescription
        return view
    }
    
    
    class func instanceInside(_ viewController: ResponsiveViewController, withTitle title: String) -> LoadingView {
        let view = LoadingView.defaultInstanceInside(viewController)
        view.setTitle(title)
        return view
    }
    
    
}





// MARK: – Accessors

extension LoadingView {
    
    
    final func runAnimation() {
        self.spinner.startAnimating()
        self.spinner.isHidden = false
    }
    
    
    final func runAnimationWithTitle(_ title: String) {
        self.setTitle(title)
        self.runAnimation()
    }
    
    
    private func setTitle(_ title: String) {
        if !title.isEmpty {
            self.titleLabel.text = title
        }
    }
    
    
    final func stopAnimation() {
        self.spinner.isHidden = true
        self.spinner.stopAnimating()
    }
    
    
}
