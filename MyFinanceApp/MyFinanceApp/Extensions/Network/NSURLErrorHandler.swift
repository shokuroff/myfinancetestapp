//
//  NSURLErrorHandler.swift
//  AA
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 AA. All rights reserved.
//



import Foundation

/// Current class serves to handle errors that might appear while working with network.

struct NSURLErrorHandler {
    
    
    typealias API = NetworkUtility.API
    
    
    // MARK: – Reasons
    
    private enum Reason: Int {
        case unknown = -1
        case cancelled = -999
        case badURL = -1000
        case timedOut = -1001
        case unsupportedURL = -1002
        case cannotFindHost = -1003
        case cannotConnectToHost = -1004
        case networkConnectionLost = -1005
        case dnsLookupFailed = -1006
        case httpTooManyRedirects = -1007
        case resourceUnavailable = -1008
        case notConnectedToInternet = -1009
        case redirectToNonExistentLocation = -1010
        case badServerResponse = -1011
        case userCancelledAuthentication = -1012
        case userAuthenticationRequired = -1013
        case zeroByteResource = -1014
        case cannotDecodeRawData = -1015
        case cannotDecodeContentData = -1016
        case cannotParseResponse = -1017
        case fileDoesNotExist = -1100
        case fileIsDirectory = -1101
        case noPermissionsToReadFile = -1102
        case secureConnectionFailed = -1200
        case serverCertificateHasBadDate = -1201
        case serverCertificateUntrusted = -1202
        case serverCertificateHasUnknownRoot = -1203
        case serverCertificateNotYetValid = -1204
        case clientCertificateRejected = -1205
        case clientCertificateRequired = -1206
        case cannotLoadFromNetwork = -2000
        case cannotCreateFile = -3000
        case cannotOpenFile = -3001
        case cannotCloseFile = -3002
        case cannotWriteToFile = -3003
        case cannotRemoveFile = -3004
        case cannotMoveFile = -3005
        case downloadDecodingFailedMidStream = -3006
        case downloadDecodingFailedToComplete = -3007
        case internationalRoamingOff = -1018
        case callIsActive = -1019
        case dataNotAllowed = -1020
        case requestBodyStreamExhausted = -1021
        case backgroundSessionRequiresSharedContainer = -995
        case backgroundSessionInUseByAnotherProcess = -996
        case backgroundSessionWasDisconnected = -997
    }
    
    
    
    
    
    // MARK: – Public properties
    
    /// Use `title` property to print or show error title to use on a screen.
    
    var title: String {
        switch self.api {
        case .updateTargetRate:
            return "Error to refresh rate"
        }
    }
    
    
    
    /// Use `message` property to print or show error description to user on a screen.
    
    var message: String {
        switch self.reason {
        case .unknown:
            return "Unknown error"
        case .cancelled:
            return "The request has been cancelled"
        case .badURL:
            return "Bad URL"
        case .timedOut:
            return "Time out"
        case .unsupportedURL:
            return "Unsupported URL"
        case .cannotFindHost:
            return "Cannot find a host"
        case .cannotConnectToHost:
            return "Cannot connect to host"
        case .networkConnectionLost:
            return "network connection lost"
        case .dnsLookupFailed:
            return "DNS Look up failed"
        case .httpTooManyRedirects:
            return "HTTP: too many redirects"
        case .resourceUnavailable:
            return "Resource is unavailable"
        case .notConnectedToInternet:
            return "Not connected to Internet"
        case .redirectToNonExistentLocation:
            return "Redirected to non-existent location"
        case .badServerResponse:
            return "Bad server responce"
        case .userCancelledAuthentication:
            return "User cancelled authentication"
        case .userAuthenticationRequired:
            return "User authentication required"
        case .zeroByteResource:
            return "Zero byte resouce"
        case .cannotDecodeRawData:
            return "Cannot decode raw data"
        case .cannotDecodeContentData:
            return "Cannot decode content data"
        case .cannotParseResponse:
            return "Cannot parse response"
        case .fileDoesNotExist:
            return "File doesn't exist"
        case .fileIsDirectory:
            return "File is a directory"
        case .noPermissionsToReadFile:
            return "No permissions to read the file"
        case .secureConnectionFailed:
            return "Secure connection failed"
        case .serverCertificateHasBadDate:
            return "Server Certificate has bad date"
        case .serverCertificateUntrusted:
            return "Server certification untrusted"
        case .serverCertificateHasUnknownRoot:
            return "Server certificate has unknown root"
        case .serverCertificateNotYetValid:
            return "Server certification not yet valid"
        case .clientCertificateRejected:
            return "Client certificate rejected"
        case .clientCertificateRequired:
            return "client cerificate required"
        case .cannotLoadFromNetwork:
            return "Cannot load from network"
        case .cannotCreateFile:
            return "Cannot create a file"
        case .cannotOpenFile:
            return "Cannot open a file"
        case .cannotCloseFile:
            return "Cannot close a file"
        case .cannotWriteToFile:
            return "Cannot write to a file"
        case .cannotRemoveFile:
            return "Cannot remote a file"
        case .cannotMoveFile:
            return "Cannot move a file"
        case .downloadDecodingFailedMidStream:
            return "Download decoding failed mid stream"
        case .downloadDecodingFailedToComplete:
            return "Download decoding failed to complete"
        case .internationalRoamingOff:
            return "International roaming off"
        case .callIsActive:
            return "Call is active"
        case .dataNotAllowed:
            return "Data not allowed"
        case .requestBodyStreamExhausted:
            return "Request body stream exhausted"
        case .backgroundSessionRequiresSharedContainer:
            return "Background session requres shared container"
        case .backgroundSessionInUseByAnotherProcess:
            return "Background session in use by another process"
        case .backgroundSessionWasDisconnected:
            return "Background session was disconnected"
        }
    }
    
    
    
    
    
    // MARK: – Private Properties
    
    private let api: API
    private let reason: Reason
    
    
    
    
    
    // MARK: – Inializer
    
    /**
     Initalizer based on `NSError` properties and case when it's can be happened.
     - Note: If `code` is out of range of the enumeration list `Reasons`, `unknown` value (-1) will be setted to `reason` property.
     - Parameter domain: A string that defines error domain.
     - Parameter code: A integer error code within error domain.
     - Parameter api: A type of API path that needs to be handled by this class.
     - Returns: Void.
     */
    
    init?(domain: String, code: Int, api: API) {
        guard domain == NSURLErrorDomain else {
            return nil
        }
        self.api = api
        self.reason = Reason(rawValue: code) ?? .unknown
    }
    
}
