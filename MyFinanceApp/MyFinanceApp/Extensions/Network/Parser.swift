//
//  Parser.swift
//  AA
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 AA. All rights reserved.
//



import Foundation




// MARK: – Structure Responses

struct UpdateTargetRateResponse: Decodable {
    let base: String // USD
    let rates: [String: Float] // ["RUB": 65.2142730339]
    let date: String // "2019-06-06"
}





// MARK: – JSON Parser Implementation

struct Parser {
    
    
    // MARK: – Typealiases
    
    typealias JSONDict = [String: Any]
    typealias API = NetworkUtility.API
    typealias SuccessParserCallback = (_ data: Any) -> Void
    typealias FailureParserCallback = (_ failure: Bool, _ error: (title: String, message: String?)?) -> Void
    
    
    
    
    
    // MARK: – Generic Parser
    
    static func parseData(_ input: Data, basedOnAPI api: API, failure: @escaping FailureParserCallback, success: @escaping SuccessParserCallback) {
        switch api {
        case .updateTargetRate:
            Parser.parseUpdateTargetRateResponse(data: input, failure: failure, success: success)
        }
    }
    
    
    
    
    
    
    // MARK: – Update Target Response Parser
    
    static func parseUpdateTargetRateResponse(data: Data, failure: @escaping FailureParserCallback, success: @escaping SuccessParserCallback) {
        do {
            let response = try JSONDecoder().decode(UpdateTargetRateResponse.self, from: data)
            success(response)
        } catch {
            #if DEBUG
            print("\n[\(self) parseUpdateTargetRateResponse:data:handler:] \(error.localizedDescription)\n")
            #endif
            failure(true, ("Bad Data", error.localizedDescription))
        }
    }
    
    
}
