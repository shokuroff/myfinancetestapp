//
//  NetworkUtility.swift
//  AA
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 AA. All rights reserved.
//



import Foundation

/// Current class serves to help with routine based on communicating with Internet servers.

public class NetworkUtility {
    
    
    typealias failureHandler = (_ failure: Bool, _ error: (title: String, message: String?)?) -> Void
    
    
    // MARK: – HTTP Methods
    
    /// Simple HTTP Methods.
    
    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case update = "UPDATE"
        case delete = "DELETE"
    }
    
    
    
    
    
    // MARK: – API paths
    
    enum API {
        /// Use this API when it's time to update target user's rate.
        case updateTargetRate
    }
    
    
    
    
    
    // MARK: – Public Properties
    
    /// Use this boolean property to define which host we need to ping - test or production version accordingly.
    /// - Note: A default value is `true`.
    
    var testMode: Bool = true
    
    final lazy var session: URLSession = {
        let session = URLSession(configuration: .default)
        return session
    }()
    
    
    
    
    
    // MARK: – Private Properties
    
    private let _testHost: String = "api.exchangeratesapi.io"
    
    private let _productionHost: String = "api.exchangeratesapi.io"
    
    private var host: String {
        return self.testMode ? _testHost : _productionHost
    }
    
    private var lastURLString: String = ""
    
    weak var router: RouterWithCallbacks!
    
    
    
    
    
    // MARK: – Object Life Cycle
    
    convenience init(withRouter router: RouterWithCallbacks) {
        self.init()
        self.router = router
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Public Methods
    
    final func tryToCreateRequest(api: API, httpMethod: HTTPMethod) -> URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = self.host
        urlComponents.path = self.makePathByAPI(api)
        guard let validURL = urlComponents.url else {
            return nil
        }
        var request = URLRequest(url: validURL)
        request.httpMethod = httpMethod.rawValue
        return request
    }
    
    
    
    final func handle(data: Data?, response: URLResponse?, error: Error?, api: API, successHandler: @escaping (Any) -> Void) {
        self.handleRawResponse(response, handler: { httpStatusCode in
            #if DEBUG
            let responseHTTPMessage = httpStatusCode == 0 ? "ERROR (0)" : "\(httpStatusCode)"
            print("\n[\(self) \(self.lastURLString)]\t\(responseHTTPMessage)\n")
            #endif
        })
        self.handlePossibleError(error, withAPI: api, handler: { failure, error in
            if failure && error != nil {
                self.router.didGetNetworkRequestErrorWithTitle(error!.title, andMessage: error!.message)
            }
        })
        self.handleRawData(data, basedOnAPI: api, failure: {failure, error in
            if failure && error != nil {
                self.router.didGetNetworkRequestErrorWithTitle(error!.title, andMessage: error!.message)
            }
        }, success: { data in
            successHandler(data)
        })
    }
    
    
    
    
    
    // MARK: – Private Methods
    
    private func makePathByAPI(_ api: API) -> String {
        switch api {
        case .updateTargetRate:
            return "/latest"
        }
    }
    
    
    
    private func handleRawResponse(_ rawResponse: URLResponse?,
                                   handler: @escaping (_ httpStatusCode: Int) -> Void) {
        
        guard let httpResponse = rawResponse as? HTTPURLResponse else {
            handler(0)
            return
        }
        handler(httpResponse.statusCode)
    }
    
    
    
    private func handlePossibleError(_ rawError: Error?, withAPI api: API, handler: @escaping failureHandler) {
        guard let someError = rawError, let error = someError as NSError? else {
            handler(false, nil) // no error 👍🏻.
            return
        }
        if let errorHandler = NSURLErrorHandler(domain: error.domain, code: error.code, api: api) {
            handler(true, (errorHandler.title, errorHandler.message))
        } else {
            handler(true, ("Unknown Error", nil))
        }
    }
    
    
    
    private func handleRawData(_ rawData: Data?, basedOnAPI api: API, failure: @escaping failureHandler, success: @escaping (_ data: Any) -> Void) {
        guard let input = rawData else {
            return
        }
        Parser.parseData(input, basedOnAPI: api, failure: { errorHappens, error in
            failure(errorHappens, error)
        }, success: { output in
            success(output)
        })
    }
    
    
}
