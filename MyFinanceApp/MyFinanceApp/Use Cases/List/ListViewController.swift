// 
//  ListViewController.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



protocol ListViewControllerWithCallbacks: ViewControllerWithCallbacks {
    func didUpdateRateAtRow(_ row: Int)
    func didRemoveRateAtRow(_ row: Int)
}



final class ListViewController: ResponsiveViewController {


	// MARK: - Properties
	
	private var viewModel: ListViewModel!
	
	
	
	
	
	// MARK: - Interface Builder Outlets
	
	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var contentContainerView: UIView!
	
	
	
	
	
	// MARK: - View Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.prepareResponsiveViewWithContentContainer(self.contentContainerView)
        self.title = "Rates"
        self.prepareTableRefreshControl()
        self.setTableView(self.tableView)
        // Setup Architecture Pattern:
		self.viewModel = ListViewModel(withViewController: self)
	}
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadDataAgain()
    }
	
	
	
	
    
	// MARK: - Interface Builder Actions
	
    @IBAction private func addButtonAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: String.showAddViewControllerSegueID, sender: self)
    }
    
    
    
    
    
    // MARK: – Additionals
    
    private func prepareTableRefreshControl() {
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(self.didRefreshControlPullDown), for: .valueChanged)
    }
    
    
    @objc override func didRefreshControlPullDown() {
        self.viewModel.reloadDataListToPresentLater()
        self.reloadTable()
    }
    
    
    override func didErrorButtonTouch() {
        self.viewState = .loading
        self.reloadDataAgain()
    }
    
    
    final private func reloadDataAgain() {
        self.viewModel.reloadDataListToPresentLater()
        if self.viewModel.noData {
            self.setErrorDescription(title: "Empty", message: "Tap '+' to choose your first rate! 👍🏻")
            self.viewState = .empty
        } else {
            self.reloadTable()
            self.viewState = .normal
        }
    }
	
	
}





// MARK: - Table View Data Source

extension ListViewController: UITableViewDataSource {
	
    
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.viewModel.sectionsCount
	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel.rowsCount
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: String.listCellID) as? ListViewCell
        self.viewModel.loadRateAtRow(indexPath.row)
        if self.viewModel.noDataForCurrentRow {
            cell?.setErrorTextEverywhere()
        } else {
            cell?.setLeftTitle(self.viewModel.leftCellTitle)
            cell?.setLeftDescription(self.viewModel.leftCellDescription)
            cell?.setRightTitle(self.viewModel.rightCellTitle)
            cell?.setRightDescription(self.viewModel.rightCellDescription)
            cell?.loads = self.viewModel.isCellLoading
            if self.viewModel.isCellLoading {
                self.viewModel.refreshCellAtRow(indexPath.row)
            }
        }
        return cell ?? UITableViewCell()
	}
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.removeCellAtRow(indexPath.row)
        }
    }
	
	
}





// MARK: - Callbacks

extension ListViewController: ListViewControllerWithCallbacks {

    
    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        if let control = self.tableView.refreshControl, control.isRefreshing {
            self.tableView.refreshControl?.endRefreshing()
        }
        self.setErrorDescription(title: title, message: rawMessage)
        self.setErrorButtonTitle("Try again")
        self.viewState = .error
    }
    
    
    func didGetNewData() {
        self.reloadTable()
        self.viewState = .normal
    }
    
    
    func didGetEmptyData() {
        if let control = self.tableView.refreshControl, control.isRefreshing {
            self.tableView.refreshControl?.endRefreshing()
        }
        self.setErrorDescription(title: "👎🏻", message: "No data has been loaded..")
        self.viewState = .empty
    }
    
    
    func didUpdateRateAtRow(_ row: Int) {
        if let control = self.tableView.refreshControl, control.isRefreshing {
            self.tableView.refreshControl?.endRefreshing()
        }
        let targetIndexPath = IndexPath(row: row, section: 0)
        self.tableView.reloadRows(at: [targetIndexPath], with: .none)
    }
    
    
    func didRemoveRateAtRow(_ row: Int) {
        self.reloadTable()
        if self.viewModel.noData {
            self.setErrorDescription(title: "Empty", message: "Tap '+' to choose your first rate! 👍🏻")
            self.viewState = .empty
        }
    }

    
}
