// 
//  ListViewModel.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



protocol ListViewModelWithCallbacks: ViewModelWithCallbacks {
    func didUpdateRateWithIndex(_ index: Int)
}



final class ListViewModel {


	// MARK: - Properties

	weak private var viewController: ListViewControllerWithCallbacks!
    private var router: ListRouter!
    private var currentLeftTitleLabelText = ""
    private var currentLeftSubtitleLabelText = ""
    private var currentRightTitleLabelText = ""
    private var currentRightSubtitleLabelText = ""
    private var currentCellIsLoading = false





	// MARK: - Object Life Cycle

	convenience init(withViewController viewController: ListViewControllerWithCallbacks) {
		self.init()
		self.viewController = viewController
        #if DEBUG
		print("\n[\(self) init]\n")
        #endif
        self.router = ListRouter(withViewModel: self)
	}


	deinit {
        #if DEBUG
		print("\n[\(self) deinit]\n")
        #endif
	}





	// MARK: - Getters

    /// The number of sections in table view.
    
    final var sectionsCount: Int {
		return self.router.sectionsCount
	}

    
    
    /// The number of rows in table view.
    
    final var rowsCount: Int {
		return self.router.rowsCount
	}
    
    
    
    /// A Boolen property indicates whether data in model is empty or not.
    
    final var noData: Bool { return self.rowsCount == 0 }
    
    
    
    /// If there is no data to present in current row at IndexPath.row, then the prperty retunrs `true`.
    
    final var noDataForCurrentRow: Bool {
        return (
            self.currentLeftTitleLabelText.isEmpty &&
            self.currentLeftSubtitleLabelText.isEmpty &&
            self.currentRightTitleLabelText.isEmpty &&
            self.currentRightSubtitleLabelText.isEmpty
        )
    }
    
    
    
    /// Boolean value regulates a loading state for current cell.
    /// It's obvious to use this property only when it's necessary to update a cell at specific row.
    
    final var isCellLoading: Bool { return self.currentCellIsLoading }
    
    
    
    /// A get-only string property for `UILabel` instance inside `ListViewCell` instance.
    
    final var leftCellTitle: String { return self.currentLeftTitleLabelText }
    
    
    
    /// A get-only string property for `UILabel` instance inside `ListViewCell` instance.
    
    final var leftCellDescription: String { return self.currentLeftSubtitleLabelText }
    
    
    
    /// A get-only string property for `UILabel` instance inside `ListViewCell` instance.
    
    final var rightCellTitle: String { return self.currentRightTitleLabelText }
    
    
    
    /// A get-only string property for `UILabel` instance inside `ListViewCell` instance.
    
    final var rightCellDescription: String { return self.currentRightSubtitleLabelText }





    // MARK: – Public Accessors
    
    /// Use this important method to load temporary data to present its for current row in table.
    /// Otherwise, current cell will be empty what seems be strange possibly.. 🧐

    final func loadRateAtRow(_ row: Int) {
        if let rate = self.router.getItemAtIndex(row) {
            // "1 USD":
            self.currentLeftTitleLabelText = "1 \(rate.baseCurrency.code)"
            // "United States Dollar":
            self.currentLeftSubtitleLabelText = rate.baseCurrency.name
            // "0.88":
            self.currentRightTitleLabelText = String(byRelativeFloatingRatio: rate.relativeRatio)
            // "Euro • EUR":
            self.currentRightSubtitleLabelText = "\(rate.secondCurrency.name) • \(rate.secondCurrency.code)"
            self.currentCellIsLoading = rate.loading
        } else {
            self.currentLeftTitleLabelText = ""
            self.currentLeftSubtitleLabelText = ""
            self.currentRightTitleLabelText = ""
            self.currentRightSubtitleLabelText = ""
        }
    }
    
    
    
    /// Use this method to refresh a relative currency ratio from server.
    /// - Parameter row: An integer value where cell needs to be updated within the table.
    
    final func refreshCellAtRow(_ row: Int) {
        self.router.refreshCellAtIndex(row)
    }
    
    
    
    /// Use this method to reload rates list each time when it's necessary to present rates list to user on the screen.
    /// - Note: A best place to call the method is `viewWillAppear` method inside view controller implementation.
    
    final func reloadDataListToPresentLater() {
        self.router.reloadRatesList()
    }
    
    
    
    /// Use this method to remove a cell at the specific row.
    /// - Parameter row: An integer value where cell needs to be removed within the table animationally.
    
    final func removeCellAtRow(_ row: Int) {
        self.router.removeRateAtIndex(row, andThenDo: {
            self.viewController.didRemoveRateAtRow(row)
        })
    }


}





// MARK: – Callbacks

extension ListViewModel: ListViewModelWithCallbacks {
    
    
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        DispatchQueue.main.async {
            self.viewController.didGetBadErrorWithTitle(title, andMessage: rawMessage)
        }
    }
    
    
    func didGetNewData() {
        DispatchQueue.main.async {
            self.viewController.didGetNewData()
        }
    }
    
    
    func didGetEmptyData() {
        DispatchQueue.main.async {
            self.viewController.didGetEmptyData()
        }
    }
    
    
    func didUpdateRateWithIndex(_ index: Int) {
        DispatchQueue.main.async {
            self.viewController.didUpdateRateAtRow(index)
        }
    }
    
    
}
