// 
//  ListModel.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



final class ListModel {
    
    
    // MARK: – Properties
    
    private var rates: [Rate] = []
    
    
    
    
    
    // MARK: – Object Life Cycle
    
    init() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Getters
    
    final var dataCount: Int {
        return self.rates.count
    }
    
    
    final var sectionsCount: Int {
        return 1
    }
    
    
    
    
    
    // MARK: – Public Accessors
    
    final func getRateAtIndex(_ index: Int) -> Rate? {
        guard index > -1 && index < self.dataCount else {
            #if DEBUG
            print("\n[\(self) getRateAtIndex] index (\(index)) is out of range.\n")
            #endif
            return nil
        }
        return self.rates[index]
    }
    
    
    final func updateRate(_ rate: Rate, completionHandler: @escaping () -> Void) {
        let ratesToUpdate = self.rates.filter { rate == $0 }
        guard ratesToUpdate.count == 1 else {
            completionHandler()
            return
        }
        let rateToUpdate = ratesToUpdate.first!
        rateToUpdate.relativeRatio = rate.relativeRatio
        rateToUpdate.loading = false
        completionHandler()
    }
    
    
    final func reloadRatesFromDatabase(andUpdateRequestsInServiceByRates callbacks: @escaping (_ rates: [Rate]) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let rates = appDelegate.coreDataStack.rates
        self.rates = rates
        callbacks(rates)
    }
    
    
    final func removeRateAtIndex(_ index: Int, andThenDo callback: @escaping (_ ratesForRequests: [Rate]) -> Void) {
        guard let rateToRemove = self.getRateAtIndex(index) else {
            self.rates.removeAll()
            callback(self.rates)
            return
        }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            self.rates.removeAll()
            callback(self.rates)
            return
        }
        appDelegate.coreDataStack.removeSpecificRate(rateToRemove, andDoAfter: { newRatesList in
            self.rates = newRatesList
            callback(self.rates)
        })
    }
    
    
}
