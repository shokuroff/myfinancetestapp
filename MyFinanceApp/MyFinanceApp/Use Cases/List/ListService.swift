// 
//  ListService.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



final class ListService: NetworkUtility {
    
    
    typealias RateRequest = (index: Int, task: URLSessionDataTask?)
    typealias ErrorDescription = (title: String, message: String?)
    typealias FailureHandler = ((_ failure: Bool, _ error: ErrorDescription?) -> Void)
    
    
    
    // MARK: – Properties
    
    private var requests: [RateRequest] = []
    
    
    
    
    
    // MARK: – Network requests
    
    final func runRequestAtIndex(_ index: Int) {
        var rawRequest: URLSessionDataTask?
        for request in self.requests {
            if request.index == index {
                rawRequest = request.task
                break
            }
        }
        guard let request = rawRequest else {
            if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorBadURL, api: .updateTargetRate) {
                self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
            }
            return
        }
        if request.state == .running {
            request.cancel()
        }
        request.resume()
    }
    
    
    
    
    
    // MARK: – Public Accessors
    
    func updateRequests(byRates rates: [Rate]) {
        self.createRequestsByRatesList(rates)
    }
    
    
    
    
    
    // MARK: – Private Methods
    
    private func createRequestsByRatesList(_ list: [Rate]) {
        var requests: [RateRequest] = []
        for (index, rate) in list.enumerated() {
            if let task = self.createRequestToUpdateRate(rate, atIndex: index) {
                let request: RateRequest = (index, task)
                requests.append(request)
            }
        }
        if !self.requests.isEmpty {
            for oldRequest in self.requests {
                oldRequest.task?.cancel()
            }
            self.requests.removeAll()
        }
        self.requests = requests
    }
    
    
    
    private func createRequestToUpdateRate(_ rate: Rate, atIndex index: Int) -> URLSessionDataTask? {
        guard let requestWithoutParameters = self.tryToCreateRequest(api: .updateTargetRate, httpMethod: .get) else {
            if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorBadURL, api: .updateTargetRate) {
                self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
            }
            return nil
        }
        guard let validURL = requestWithoutParameters.url else {
            if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorBadURL, api: .updateTargetRate) {
                self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
            }
            return nil
        }
        var urlComponents = URLComponents(string: validURL.absoluteString)
        let firstParameter = URLQueryItem(name: "base", value: rate.baseCurrency.code)
        let secondParameter = URLQueryItem(name: "symbols", value: rate.secondCurrency.code)
        urlComponents?.queryItems = [firstParameter, secondParameter]
        guard let urlWithParameters = urlComponents?.url else {
            if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorBadURL, api: .updateTargetRate) {
                self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
            }
            return nil
        }
        let requestWithParameters = URLRequest(url: urlWithParameters)
        let task = self.session.dataTask(with: requestWithParameters) { data, response, error in
            self.handle(data: data, response: response, error: error, api: .updateTargetRate, successHandler: { someData in
                let serverResponse = someData as! UpdateTargetRateResponse
                guard rate.baseCurrency.code == serverResponse.base else {
                    if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorCannotDecodeRawData, api: .updateTargetRate) {
                        self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
                    }
                    return
                }
                guard let newRelativeRatio = serverResponse.rates[rate.secondCurrency.code] else {
                    if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorCannotDecodeRawData, api: .updateTargetRate) {
                        self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
                    }
                    return
                }
                let rateToUpdate = rate
                rateToUpdate.relativeRatio = newRelativeRatio
                if let listRouter = self.router as? ListRouterWithCallbacks {
                    listRouter.didUpdateRate(rateToUpdate, atIndex: index)
                } else {
                    if let error = NSURLErrorHandler(domain: NSURLErrorDomain, code: NSURLErrorFileDoesNotExist, api: .updateTargetRate) {
                        self.router.didGetNetworkRequestErrorWithTitle(error.title, andMessage: error.message)
                    }
                }
            })
        }
        return task
    }
    
    
}
