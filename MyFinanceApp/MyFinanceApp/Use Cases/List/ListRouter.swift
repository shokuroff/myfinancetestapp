// 
//  ListRouter.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



protocol ListRouterWithCallbacks: RouterWithCallbacks {
    func didUpdateRate(_ rate: Rate, atIndex index: Int)
}



final class ListRouter {
    
    
    // MARK: – Properties
    
    weak final private var viewModel: ListViewModelWithCallbacks!
    final private var model: ListModel
    final private var service: ListService!
    
    
    
    
    
    // MARK: – Object Life Cycle
    
    init() {
        self.model = ListModel()
    }
    
    
    convenience init(withViewModel viewModel: ListViewModelWithCallbacks) {
        self.init()
        self.viewModel = viewModel
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
        self.service = ListService(withRouter: self)
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Getters
    
    final var sectionsCount: Int {
        return self.model.sectionsCount
    }
    
    
    final var rowsCount: Int {
        return self.model.dataCount
    }
    
    
    
    
    
    // MARK: – Public Accessors
    
    final func getItemAtIndex(_ index: Int) -> Rate? {
        return self.model.getRateAtIndex(index)
    }
    
    
    final func refreshCellAtIndex(_ index: Int) {
        self.service.runRequestAtIndex(index)
    }
    
    
    final func reloadRatesList() {
        self.model.reloadRatesFromDatabase(andUpdateRequestsInServiceByRates: { ratesToUpdate in
            self.service.updateRequests(byRates: ratesToUpdate)
        })
    }
    
    
    final func removeRateAtIndex(_ index: Int, andThenDo callback: @escaping () -> Void) {
        self.model.removeRateAtIndex(index, andThenDo: { ratesForRequests in
            self.service.updateRequests(byRates: ratesForRequests)
            callback()
        })
    }
    
    
}





// MARK: – Network Callbacks

extension ListRouter: ListRouterWithCallbacks {
    
    
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewModel.didGetNetworkRequestErrorWithTitle(title, andMessage: rawMessage)
    }
    
    
    func didLoadFromNetworkData(_ data: [Any]) {
        
    }
    
    
    func didUpdateRate(_ rate: Rate, atIndex index: Int) {
        self.model.updateRate(rate, completionHandler: {
            self.viewModel.didUpdateRateWithIndex(index)
        })
    }
    
    
}
