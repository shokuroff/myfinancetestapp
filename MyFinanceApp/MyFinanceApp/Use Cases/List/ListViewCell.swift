//
//  ListCell.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/7/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



class ListViewCell: UITableViewCell {
    
    
    // MARK: – Properties
    
    private var loadingMode: Bool = false
    private var row: Int = 0
    private let errorDefaultText = "Error 🤖"
    
    
    
    
    
    // MARK: – Interface Builder Outlets
    
    @IBOutlet weak private var leftTitleLabel: UILabel!
    @IBOutlet weak private var leftDescriptionLabel: UILabel!
    @IBOutlet weak private var rightTitleLabel: UILabel!
    @IBOutlet weak private var rightDescriptionLabel: UILabel!
    @IBOutlet weak private var spinner: UIActivityIndicatorView!
    
    
    
    
    
    // MARK: – Rendering
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    
    
    
    
    // MARK: – Public Properties
    
    var loads: Bool {
        get {
            return self.loadingMode
        }
        set(newState) {
            defer { self.updateView() }
            self.loadingMode = newState
        }
    }
    
    
    
    
    
    // MARK: – Getters
    
    private var labels: [UILabel] {
        return [leftTitleLabel, leftDescriptionLabel, rightTitleLabel, rightDescriptionLabel]
    }
    
    
    
    
    
    // MARK: - Setters
    
    final func setLeftTitle(_ title: String) {
        self.leftTitleLabel.text = title.isEmpty ? self.errorDefaultText : title
    }
    
    
    final func setLeftDescription(_ description: String) {
        self.leftDescriptionLabel.text = description.isEmpty ? self.errorDefaultText : description
    }
    
    
    final func setRightTitle(_ title: String) {
        self.rightTitleLabel.text = title.isEmpty ? self.errorDefaultText : title
    }
    
    
    final func setRightDescription(_ description: String) {
        self.rightDescriptionLabel.text = description.isEmpty ? self.errorDefaultText : description
    }
    
    
    final func setErrorTextEverywhere() {
        self.labels.forEach { $0.text = self.errorDefaultText }
    }
    
    
    
    
    
    // MARK: – Private Methods
    
    private func updateView() {
        switch self.loadingMode {
        case true:
            self.rightTitleLabel.isHidden = true
            self.spinner.startAnimating()
            self.spinner.isHidden = false
        case false:
            self.spinner.isHidden = true
            self.spinner.startAnimating()
            self.rightTitleLabel.isHidden = false
        }
    }
    
    
}
