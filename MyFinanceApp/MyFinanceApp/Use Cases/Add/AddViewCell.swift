//
//  AddViewCell.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/7/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



class AddViewCell: UITableViewCell {
    
    
    // MARK: – Properties
    
    private let errorDefaultText = "Error 🤖"
    
    
    
    
    
    // MARK: – Getters
    
    final private lazy var codeLabelCreatedProgrammatically: UILabel = {
        defer {
            self.setNeedsDisplay()
        }
        let frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 19.5)
        let label = UILabel(frame: frame)
        label.addConstraintsForCodeCurrencyLabelInsideContentView(self.contentView)
        label.setupTextStyleForCodeCurrencyLabel()
        return label
    }()
    
    
    final private lazy var nameLabelCreatedProgrammatically: UILabel = {
        defer {
            self.setNeedsDisplay()
        }
        let frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 19.5)
        let label = UILabel(frame: frame)
        label.addConstraintsForNameCurrencyLabelInsideContentView(self.contentView)
        label.setupTextStyleForNameCurrencyLabel()
        label.leftAnchor.constraint(equalTo: self.codeLabelCreatedProgrammatically.rightAnchor, constant: 16.0).isActive = true
        return label
    }()
    
    
    final private var currentCodeLabel: UILabel {
        return self.codeLabel ?? self.codeLabelCreatedProgrammatically
    }
    
    
    final private var currentNameLabel: UILabel {
        return self.nameLabel ?? self.nameLabelCreatedProgrammatically
    }
    
    
    
    
    
    // MARK: – Interface Builder Outlets
    
    @IBOutlet weak private var codeLabel: UILabel!
    @IBOutlet weak private var nameLabel: UILabel!
    
    
    
    
    
    // MARK: – Rendering
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    
    
    
    
    // MARK: – Getters
    
    var labels: [UILabel] {
        get {
            return [self.currentCodeLabel, self.currentNameLabel]
        }
    }
    
    
    
    
    
    // MARK: – Setters
    
    final func setName(_ name: String, andCode code: String) {
        defer {
            self.setNeedsDisplay()
        }
        guard !name.isEmpty && !code.isEmpty else {
            self.setErrorTextEverywhere()
            return
        }
        self.currentNameLabel.text = name
        self.currentCodeLabel.text = code
    }
    
    
    final func setErrorTextEverywhere() {
        defer {
            self.setNeedsDisplay()
        }
        self.labels.forEach { $0.text = self.errorDefaultText }
    }
    
    
}
