// 
//  AddRouter.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



final class AddRouter {
    
    
    typealias ErrorDescription = (title: String, message: String?)
    typealias FailureHandler = ((_ failure: Bool, _ error: ErrorDescription?) -> Void)
    
    
    
    // MARK: – Properties
    
    weak final private var viewModel: ViewModelWithCallbacks!
    final private var model: AddModel
    
    
    
    
    
    // MARK: – Object Life Cycle
    
    init() {
        self.model = AddModel()
    }
    
    
    convenience init(withViewModel viewModel: ViewModelWithCallbacks) {
        self.init()
        self.viewModel = viewModel
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Getters
    
    final var sectionsCount: Int {
        return self.model.sectionsCount
    }
    
    
    final var rowsCount: Int {
        return self.model.dataCount
    }
    
    
    
    
    
    // MARK: – Setters
    
    final func fixBaseCurrencyAtIndex(_ index: Int) {
        self.model.fixBaseCurrencyAtIndex(index)
    }
    
    
    final func fixSecondCurrencyAtIndex(_ index: Int) {
        self.model.fixSecondCurrencyAtIndex(index)
    }
    
    
    final func delesectBaseCurrency() {
        self.model.delesectBaseCurrency()
    }
    
    
    final func deselectSecondCurrency() {
        self.model.deselectSecondCurrency()
    }
    
    
    
    
    
    // MARK: – Public Accessors

    final func loadCurrencyAtIndex(_ index: Int) -> Currency? {
        return self.model.getCurrencyAtIndex(index)
    }
    
    
    final func saveNewRate(andDoAfterCompletion callback: @escaping FailureHandler) {
        self.model.tryToSaveNewRate(andDoAfter: { failure, possibleErrorDescription in
            callback(failure, possibleErrorDescription)
        })
    }
    
    
}





// MARK: – Network Callbacks

extension AddRouter: RouterWithCallbacks {
    
    
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewModel.didGetNetworkRequestErrorWithTitle(title, andMessage: rawMessage)
    }
    
    
    func didLoadFromNetworkData(_ data: [Any]) {}
    
    
}
