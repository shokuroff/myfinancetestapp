// 
//  AddViewModel.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import Foundation



final class AddViewModel {
    
    
    typealias ErrorDescription = (title: String, message: String?)
    typealias FailureHandler = ((_ failure: Bool, _ error: ErrorDescription?) -> Void)
    


	// MARK: - Properties

	weak private var viewController: ViewControllerWithCallbacks!
    private var router: AddRouter!
    private var temporaryCurrentCodeCurrency: String = ""
    private var temporaryCurrentNameCurrency: String = ""





	// MARK: - Object Life Cycle

	convenience init(withViewController viewController: ViewControllerWithCallbacks) {
		self.init()
		self.viewController = viewController
        #if DEBUG
		print("\n[\(self) init]\n")
        #endif
        self.router = AddRouter(withViewModel: self)
	}


	deinit {
        #if DEBUG
		print("\n[\(self) deinit]\n")
        #endif
	}





	// MARK: - Getters

    /// An integer value indicating the number of sections in table view.
    
    final var sectionsCount: Int { return self.router.sectionsCount }
    
    
    
    /// An integer value indicating the number of rows in table view.
    
    final var rowsCount: Int { return self.router.rowsCount }
    
    
    
    /// A boolean value indicates whether does it have any data to present in table view or doesn't.
    
    final var noData: Bool { return self.rowsCount == 0 }
    
    
    
    /// Use this get-only property to present a string in `AddViewCell` instance.
    
    final var currencyCode: String { return self.temporaryCurrentCodeCurrency }
    
    
    
    /// Use this get-only property to present a string in `AddViewCell` instance.
    
    final var currencyName: String { return self.temporaryCurrentNameCurrency }
    
    
    
    
    // MARK: – Setters
    
    /// Use this method to **fix** user's selection of base currency from the given list in the table.
    final func fixBaseCurrencyAtRow(_ row: Int) {
        self.router.fixBaseCurrencyAtIndex(row)
    }
    
    
    
    /// Use this method to **fix** user's selection of second currency from the given list in the table.
    
    final func fixSecondCurrencyAtRow(_ row: Int) {
        self.router.fixSecondCurrencyAtIndex(row)
    }
    
    
    
    /// Use this method to **reject** user's selection of base currency from the given list in the table.
    
    final func delesectBaseCurrency() {
        self.router.delesectBaseCurrency()
    }
    
    
    
    /// Use this method to **reject** user's selection of second currency from the given list in the table.
    
    final func deselectSecondCurrency() {
        self.router.deselectSecondCurrency()
    }





    // MARK: – Public Accessors
    
    /// Use this important method to load temporary data to present its for current row in table.
    /// Otherwise, current cell will be empty what seems be strange possibly.. 🧐

    final func loadCurrencyAtRow(_ row: Int, andAfterPresentCell handler: @escaping (_ success: Bool) -> Void) {
        if let temporaryCurrency = self.router.loadCurrencyAtIndex(row) {
            self.temporaryCurrentCodeCurrency = temporaryCurrency.code
            self.temporaryCurrentNameCurrency = temporaryCurrency.name
            handler(true)
        } else {
            handler(false)
        }
    }
    
    
    
    /// Use this method to save a new user's rate based on two temporary selected currencies by user from the given lists.
    /// - Note: Only use if base and second currencies have been chosen by user just right before. Otherwise, user will see the error on a screen. 💩
    
    final func tryToSaveNewRate(andDoAfterDependingOnResultsHere callback: @escaping FailureHandler) {
        self.router.saveNewRate(andDoAfterCompletion: { failure, possibleErrorDescription in
            callback(failure, possibleErrorDescription)
        })
    }


}





// MARK: – Callbacks

extension AddViewModel: ViewModelWithCallbacks {
    
    
    func didGetNetworkRequestErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.viewController.didGetBadErrorWithTitle(title, andMessage: rawMessage)
    }
    
    
    func didGetNewData() {
        self.viewController.didGetNewData()
    }
    
    
    func didGetEmptyData() {
        self.viewController.didGetEmptyData()
    }
    
    
}
