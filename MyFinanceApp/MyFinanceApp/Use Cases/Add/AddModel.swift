// 
//  AddModel.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



final class AddModel {
    
    
    typealias ErrorDescription = (title: String, message: String?)
    typealias FailureHandler = ((_ failure: Bool, _ error: ErrorDescription?) -> Void)

    
    
    // MARK: – Properties
    
    private var currencies: [Currency] = []
    
    
    
    
    
    // MARK: – Object Life Cycle
    
    init() {
        #if DEBUG
        print("\n[\(self) init]\n")
        #endif
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            self.currencies = appDelegate.coreDataStack.currencies
        }
    }
    
    
    deinit {
        #if DEBUG
        print("\n[\(self) deinit]\n")
        #endif
    }
    
    
    
    
    
    // MARK: – Getters
    
    final var dataCount: Int {
        return self.currencies.count
    }
    
    
    final var sectionsCount: Int {
        return 1
    }
    
    
    
    
    
    // MARK: – Setters
    
    final func fixBaseCurrencyAtIndex(_ index: Int) {
        let currencyToFix = self.getCurrencyAtIndex(index)
        self.fixCurrency(currencyToFix, isBaseCurrency: true)
    }
    
    
    final func fixSecondCurrencyAtIndex(_ index: Int) {
        let currencyToFix = self.getCurrencyAtIndex(index)
        self.fixCurrency(currencyToFix, isBaseCurrency: false)
    }
    
    
    final func delesectBaseCurrency() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("\n[\(self) delesectBaseCurrency] App Delegate is not available.\n")
            return
        }
        appDelegate.coreDataStack.deselectBaseCurrency()
    }
    
    
    final func deselectSecondCurrency() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("\n[\(self) deselectSecondCurrency] App Delegate is not available.\n")
            return
        }
        appDelegate.coreDataStack.deselectSecondCurrency()
    }
    
    
    
    
    
    // MARK: – Public Accessors
    
    final func getCurrencyAtIndex(_ index: Int) -> Currency? {
        guard index > -1 && index < self.dataCount else {
            #if DEBUG
            print("\n[\(self) getCurrencyAtIndex:] Given index (\(index)) is out of range.\n")
            #endif
            return nil
        }
        return self.currencies[index]
    }
    
    
    final func tryToSaveNewRate(andDoAfter callback: @escaping FailureHandler) {
        let errorDefaultMessage = "Tech bug inside app.\nPlease, report developers about this problem attaching screenshot with this message. 🤝"
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("\n[\(self) tryToSaveNewRate] App Delegate is not available.\n")
            callback(true, (String.failureWithSavingNewRateTitle, errorDefaultMessage))
            return
        }
        appDelegate.coreDataStack.tryToSaveNewRate(andDoAfterCompletion: { failure, possibleErrorDescription in
            callback(failure, possibleErrorDescription)
        })
    }
    
    
    
    
    
    // MARK: – Private Methods
    
    private func fixCurrency(_ currencyToFix: Currency?, isBaseCurrency: Bool) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("\n[\(self) fixCurrency:isBaseCurrency:(\(isBaseCurrency))] App Delegate is not available.\n")
            return
        }
        switch isBaseCurrency {
        case true: appDelegate.coreDataStack.fixBaseCurrency(currencyToFix)
        case false: appDelegate.coreDataStack.fixSecondCurrency(currencyToFix)
        }
    }
    
    
}
