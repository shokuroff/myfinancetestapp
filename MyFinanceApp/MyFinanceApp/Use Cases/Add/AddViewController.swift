// 
//  AddViewController.swift
//  MyFinanceApp
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//



import UIKit



final class AddViewController: ResponsiveViewController {


	// MARK: - Properties
	
	var viewModel: AddViewModel!
    
    
    
    /// This boolean property indicates whether it's a view controller for selection
    /// base currency or not, otherwise, for second currency.
    /// - Note: It's the important property.
    var presentsBaseCurrenciesList = true
    
    
    
    /// This container view is for **second** currencies list.
    private lazy var secondContentContainerView: UIView = {
        let container = UIView(frame: self.view.bounds)
        self.view.addSubview(container)
        container.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return container
    }()
    
    
    
    /// This table view is for **second** currencies list.
    private lazy var secondTableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .singleLine
        table.backgroundColor = .clear
        self.secondContentContainerView.addSubview(table)
        table.frame = self.view.bounds
        table.delegate = self
        table.dataSource = self
        table.register(AddViewCell.self, forCellReuseIdentifier: String.addCellID)
        table.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return table
    }()
    
    
    
    /// Use this important get-only property which dynamically indicates
    /// which one table it needs to be used depending on presenting currencies list
    /// whether it's a base or second currencies list currently.
    private var currentTableView: UITableView {
        get {
            switch self.presentsBaseCurrenciesList {
            case true: return self.tableView
            case false: return self.secondTableView
            }
        }
    }
    
    
    
    /// Use this important get-only property which dynamically indicates
    /// which one container view it needs to be used depending on presenting currencies list
    /// whether it's a base or second currencies list currently.
    private var currentContainerView: UIView {
        get {
            switch self.presentsBaseCurrenciesList {
            case true: return self.contentContainerView
            case false: return self.secondContentContainerView
            }
        }
    }
    
    
    
    /// Use this dynamic property to indicate current title to present in view controller.
    private var currentTitle: String {
        get {
            switch self.presentsBaseCurrenciesList {
            case true: return "First Currency"
            case false: return "Second Currency"
            }
        }
    }
	
	
	
	
	// MARK: - Interface Builder Outlets
	
    /// This table view is for **base** currencies list.
	@IBOutlet weak private var tableView: UITableView!
    /// This container view is for **base** currencies list.
    @IBOutlet weak private var contentContainerView: UIView!
	
	
	
	
	
	// MARK: - View Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
        self.title = self.currentTitle
        self.prepareResponsiveViewWithContentContainer(self.currentContainerView)
        self.currentTableView.delegate = self
        self.currentTableView.dataSource = self
        self.setTableView(self.currentTableView)
        self.viewModel = AddViewModel(withViewController: self)
	}
	
	
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.viewModel.noData {
            self.setErrorDescription(title: "Empty", message: "Choose your first rate! 👍🏻")
            self.viewState = .empty
        } else {
            self.reloadTable()
            self.viewState = .normal
        }
    }
    
    
    deinit {
        switch self.presentsBaseCurrenciesList {
        case true:
            self.viewModel.delesectBaseCurrency()
        case false:
            self.viewModel.deselectSecondCurrency()
        }
    }
	
	
}





// MARK: - Table View Data Source and Delegate

extension AddViewController: UITableViewDataSource, UITableViewDelegate {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.viewModel.sectionsCount
	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel.rowsCount
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.addCellID) as? AddViewCell
        self.viewModel.loadCurrencyAtRow(indexPath.row, andAfterPresentCell: { success in
            if success {
                cell?.setName(self.viewModel.currencyName, andCode: self.viewModel.currencyCode)
            } else {
                cell?.setErrorTextEverywhere()
            }
        })
        return cell ?? UITableViewCell()
	}
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        switch self.presentsBaseCurrenciesList {
        case true:
            self.viewModel.fixBaseCurrencyAtRow(indexPath.row)
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let addViewController = mainStoryboard.instantiateViewController(withIdentifier: "AddViewControllerIdentifier") as? AddViewController {
                addViewController.presentsBaseCurrenciesList = false
                self.navigationController?.pushViewController(addViewController, animated: true)
            }
        case false:
            self.viewModel.fixSecondCurrencyAtRow(indexPath.row)
            self.viewModel.tryToSaveNewRate(andDoAfterDependingOnResultsHere: { failure, error in
                if failure && error != nil {
                    let alert = UIAlertController(
                        title: error!.title,
                        message: error!.message,
                        preferredStyle: .alert
                    )
                    let ok = UIAlertAction(title: "O-o-kay.. 😤", style: .default)
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                }
                if !failure {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            })
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.presentsBaseCurrenciesList ? UITableView.automaticDimension : 53.0
    }
	
	
}





// MARK: - Callbacks

extension AddViewController: ViewControllerWithCallbacks {

    
    func didGetBadErrorWithTitle(_ title: String, andMessage rawMessage: String?) {
        self.setErrorDescription(title: title, message: rawMessage)
        self.setErrorButtonTitle("Try again")
        self.viewState = .error
    }
    
    
    func didGetNewData() {
        self.reloadTable()
        self.viewState = .normal
    }
    
    
    func didGetEmptyData() {
        self.setErrorDescription(title: "👎🏻", message: "No data has been loaded..")
        self.viewState = .empty
    }

    
}
