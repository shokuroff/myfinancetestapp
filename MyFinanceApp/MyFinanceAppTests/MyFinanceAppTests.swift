//
//  MyFinanceAppTests.swift
//  MyFinanceAppTests
//
//  Created by Ivan Shokurov on 6/6/19.
//  Copyright © 2019 Ivan Shokurov. All rights reserved.
//

import XCTest
@testable import MyFinanceApp

class MyFinanceAppTests: XCTestCase {
    
    
    typealias Test1 = (input: Float, expected: String)
    
    
    // MARK: – Properties
    
    private let modelName: String = "MyFinanceApp"
    private var coreDataStack: CoreDataStack!
    
    private let test1a: Test1 = (13.312647, "13.31")
    private let test1b: Test1 = (13, "13.00")
    private let test1c: Test1 = (13.0013, "13.00")
    private let test1d: Test1 = (13.1, "13.10")
    
    var usDollar: Currency? {
        return self.coreDataStack.currencies
            .filter { $0.code == "USD" }
            .first
    }
    
    var russianRuble: Currency? {
        return self.coreDataStack.currencies
            .filter { $0.code == "RUB" }
            .first
    }
    
    var icelandicKrone: Currency? {
        return self.coreDataStack.currencies
            .filter { $0.code == "ISK" }
            .first
    }
    
    var euro: Currency? {
        return self.coreDataStack.currencies
            .filter { $0.code == "EUR" }
            .first
    }
    
    
    
    
    // MARK: – Test Cycles
    
    override func setUp() {
        self.coreDataStack = CoreDataStack(modelName: self.modelName)
    }
    
    
    override func tearDown() {
        if !self.coreDataStack.rates.isEmpty {
            self.coreDataStack.clearAllRates()
        }
    }
    
    

    
    
    // - MARK: – TEST 1
    // - Description: init:byRelativeFloatingRatio (String extension).
    
    /// Test 1a:
    func testConvertingFloatValueToCorrectString1() {
        let output = String(byRelativeFloatingRatio: self.test1a.input)
        XCTAssertEqual(self.test1a.expected, output)
    }
    
    
    /// Test 1b:
    func testConvertingFloatValueToCorrectString2() {
        let output = String(byRelativeFloatingRatio: self.test1b.input)
        XCTAssertEqual(self.test1b.expected, output)
    }
    
    
    /// Test 1c:
    func testConvertingFloatValueToCorrectString3() {
        let output = String(byRelativeFloatingRatio: self.test1c.input)
        XCTAssertEqual(self.test1c.expected, output)
    }
    
    
    /// Test 1d:
    func testConvertingFloatValueToCorrectString4() {
        let output = String(byRelativeFloatingRatio: self.test1d.input)
        XCTAssertEqual(self.test1d.expected, output)
    }
    
    
    
    
    
    // - MARK: – TEST 2
    // - Description: Working with CDCurrency.
    
    /// Test 2a:
    /// - Returns: (Bool) Does it remove all currencies from database if it's necessary?
    func testCurrenciesInCoreData1() {
        self.coreDataStack.clearAllCurrencies()
        XCTAssertTrue(self.coreDataStack.currencyListEmpty)
    }
    
    
    /// Test 2b:
    /// Let's image the moment when app launches firstly:
    /// - Returns: (Bool) Does it load currencies from raw text file?
    func testCurrenciesInCoreData2() {
        XCTAssertFalse(self.coreDataStack.currencyListEmpty)
    }
    
    
    
    
    
    // - MARK: – TEST 3
    // - Description: Checking `Equatable` protocol conformance
    // for `Currency` and `Rate` structures.
    
    /// Test 3a:
    /// Comparison two same currencies (USD).
    /// - Returns: (Bool) Does equatable protocol work on `Currency` structure?
    func testCurrencies1() {
        XCTAssertEqual(self.usDollar!, self.usDollar!)
    }
    
    
    /// Test 3b:
    /// Comparison two different currencies (USD and RUB).
    /// - Returns: (Bool) Does equatable protocol work on `Currency` structure?
    func testCurrencies2() {
        XCTAssertNotEqual(self.usDollar!, self.russianRuble!)
    }
    
    
    /// Test 3c:
    /// Comparison two same rates.
    /// - Returns: (Bool) Does equatable protocol work on `Rate` structure?
    func testCurrencies3() {
        let rate1 = self.createTestRateWithUSDDollarAndRussianRuble()
        let rate2 = self.createTestRateWithUSDDollarAndRussianRuble()
        XCTAssertEqual(rate1, rate2)
    }
    
    
    /// Test 3d:
    /// Comparison two different rates.
    /// - Returns: (Bool) Does equatable protocol work on `Rate` structure?
    func testCurrencies4() {
        let rate1 = self.createTestRateWithEuroAndIcelandicKrone()
        let rate2 = self.createTestRateWithUSDDollarAndRussianRuble()
        XCTAssertNotEqual(rate1, rate2)
    }
    
    
    
    
    
    // - MARK: – TEST 4
    // - Description: Working with CDRate/Rate model.
    
    /// Test 4a:
    /// The moment when app launches firstly:
    /// - Returns: (Bool) Is it no user's data while loading current app for the first time?
    func testRatesInCoreData1() {
        XCTAssertTrue(self.coreDataStack.rates.isEmpty)
    }
    
    
    /// Test 4b:
    /// Is it real to select U.S. dollar from the list?
    /// - Returns: (Bool) Is it possible to select U.S. dollar?
    func testRatesInCoreData2() {
        XCTAssertNotNil(self.usDollar)
    }
    
    
    /// Test 4c:
    /// Is it real to select Russian ruble from the list?
    /// - Returns: (Bool) Is it possible to select Russian Ruble?
    func testRatesInCoreData3() {
        XCTAssertNotNil(self.russianRuble)
    }
    
    
    /// Test4d:
    /// Is it real to select Icelandic krone from the list?
    /// - Returns: (Bool) Is it possible to select Icelandic Krone?
    func testRatesInCoreData4() {
        XCTAssertNotNil(self.icelandicKrone)
    }
    
    
    /// Test4e:
    /// Is it real to select Euro from the list?
    /// - Returns: (Bool) Is it possible to select EURO?
    func testRatesInCoreData5() {
        XCTAssertNotNil(self.euro)
    }
    
    
    /// Test 4f:
    /// Trying to save a new rate. (USD-RUB)
    /// - Returns: (Bool) Does it save a new USD-RUB rate?
    func testRatesInCoreData6() {
        let rateToSave = self.createTestRateWithUSDDollarAndRussianRuble()
        self.coreDataStack.saveNewRate(rateToSave, callback: { _, _ in })
        XCTAssertEqual(self.coreDataStack.rates.count, 1)
    }
    
    
    /// Test 4g:
    /// Trying to save the same rate one more time. (USD-RUB)
    /// - Returns: (Bool) Does it save a new USD-RUB rate **TWICE**?
    func testRatesInCoreData7() {
        let rateToSave = self.createTestRateWithUSDDollarAndRussianRuble()
        self.coreDataStack.saveNewRate(rateToSave, callback: { _, _ in})
        self.coreDataStack.saveNewRate(rateToSave, callback: { _, _ in})
        XCTAssertEqual(self.coreDataStack.rates.count, 1)
    }
    
    
    /// Test 4h:
    /// Trying to save 2nd user's rate (EUR-ISK)
    /// - Returns: (Bool) Is database keeping two user's rates currently?
    func testRatesInCoreData8() {
        let firstRate = self.createTestRateWithUSDDollarAndRussianRuble()
        let secondRate = self.createTestRateWithEuroAndIcelandicKrone()
        self.coreDataStack.saveNewRate(firstRate, callback: { _, _ in})
        self.coreDataStack.saveNewRate(secondRate, callback: { _, _ in})
        XCTAssertEqual(self.coreDataStack.rates.count, 2)
    }
    
    
    
    /// Test 4i:
    /// Trying to clear all user's rates.
    /// - Returns: (Bool) Has app just cleared all user's data (rates)?
    func testRatesInCoreData9() {
        let firstRate = self.createTestRateWithUSDDollarAndRussianRuble()
        let secondRate = self.createTestRateWithEuroAndIcelandicKrone()
        self.coreDataStack.saveNewRate(firstRate, callback: { _, _ in})
        self.coreDataStack.saveNewRate(secondRate, callback: { _, _ in})
        self.coreDataStack.clearAllRates()
        XCTAssertTrue(self.coreDataStack.rates.isEmpty)
    }
    
    
    
    /// Test 4j:
    /// Trying to clear a specific user's rate (USD-RUB).
    /// - Returns: (Bool) Has app just clear a specific user's rate(USD-RUB)?
    func testRatesInCoreData10() {
        let firstRate = self.createTestRateWithUSDDollarAndRussianRuble()
        let secondRate = self.createTestRateWithEuroAndIcelandicKrone()
        self.coreDataStack.saveNewRate(firstRate, callback: { _, _ in})
        self.coreDataStack.saveNewRate(secondRate, callback: { _, _ in})
        #if DEBUG
        print("\n\n\(self.coreDataStack.rates)\n\n")
        #endif
        self.coreDataStack.removeSpecificRate(secondRate, andDoAfter: { _ in })
        #if DEBUG
        print("\n\n\(self.coreDataStack.rates)\n\n")
        #endif
        XCTAssertEqual(self.coreDataStack.rates.count, 1)
    }
    
    
    
    /// Test 4k:
    /// Trying to clear a specific user's rate which is not in the list database.
    /// - Returns: (Bool) Does app ignore non-existing rate to remote its from database?
    func testRatesInCoreData11() {
        let firstRate = self.createTestRateWithUSDDollarAndRussianRuble()
        let secondRate = self.createTestRateWithEuroAndIcelandicKrone()
        self.coreDataStack.saveNewRate(firstRate, callback: { _, _ in})
        #if DEBUG
        print("\n\n\(self.coreDataStack.rates)\n\n")
        #endif
        self.coreDataStack.removeSpecificRate(secondRate, andDoAfter: { _ in })
        #if DEBUG
        print("\n\n\(self.coreDataStack.rates)\n\n")
        #endif
        XCTAssertEqual(self.coreDataStack.rates.count, 1)
    }
    
    
    
    /// Test 4l:
    /// Trying to save two temporary selected currencies as a new user's rate in database.
    /// - Returns: (Bool) Does app save a new rate based on two temporary selected currencies successfully?
    func testRatesInCoreData12() {
        let ruble = self.russianRuble
        let dollar = self.usDollar
        self.coreDataStack.fixBaseCurrency(dollar)
        self.coreDataStack.fixSecondCurrency(ruble)
        self.coreDataStack.tryToSaveNewRate(andDoAfterCompletion: { failure, error in
            #if DEBUG
            print("\n\n\(self.coreDataStack.rates)\n\n")
            #endif
            XCTAssertEqual(self.coreDataStack.rates.count, 1)
        })
    }
    
    
    
    
    
    // MARK: – Helpers
    
    private func createTestRateWithUSDDollarAndRussianRuble() -> Rate {
        return Rate(baseCurrency: self.usDollar!, secondCurrency: self.russianRuble!, relativeValue: 56.884356)
    }
    
    private func createTestRateWithEuroAndIcelandicKrone() -> Rate {
        return Rate(baseCurrency: self.euro!, secondCurrency: self.icelandicKrone!, relativeValue: 139.3)
    }
    

}
